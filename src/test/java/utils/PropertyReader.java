package utils;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class PropertyReader {

    Properties properties;


    private static final PropertyReader instance = new PropertyReader();

    public static PropertyReader getInstance(){
        return instance;
    }


    public String getAuthUrl() {
        return properties.getProperty("authUrl");
    }


    public String getEndpointA() {
        return properties.getProperty("endpointA");
    }


    public String getEndpointB() {
        return properties.getProperty("endpointB");
    }


    private PropertyReader(){
        try(InputStream inputStream = this.getClass().getClassLoader().getResourceAsStream(System.getProperty("app.env")+".properties")){

            properties = new Properties();
            properties.load(inputStream);


        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
